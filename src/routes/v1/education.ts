import { Router } from "express";
import { createEducation } from "../../common/v1/createEducationEntry";
import { deleteEducation } from "../../common/v1/deleteEducationEntry";
import { editEducation } from "../../common/v1/editEducationEntry";
import {
  getEducation,
  getEducationById,
} from "../../common/v1/getEducationEntries";

export const educationV1Router = Router();

educationV1Router.get("/education", getEducation);
educationV1Router.get("/education/:id", getEducationById);
educationV1Router.post("/education", createEducation);
educationV1Router.put("/education/:id", editEducation);
educationV1Router.delete("/education/:id", deleteEducation);
