import { CompressionTypes, Kafka, logLevel, Message, Producer } from "kafkajs";

export class KafkaLogger {
  private kafka: Kafka;
  private producer: Producer;
  private isProduerConnected: boolean;

  constructor(brokers: Array<string>) {
    this.kafka = new Kafka({
      brokers: [...brokers],
      logLevel: logLevel.ERROR,
    });
    this.producer = this.kafka.producer();
    this.isProduerConnected = false;

    this.producer.on(
      "producer.connect",
      () => (this.isProduerConnected = true)
    );
  }

  public async connect(): Promise<boolean> {
    try {
      await this.producer.connect();
      return true;
    } catch (err) {
      console.log("[KAFKA Error] ", err);
      return false;
    }
  }

  public async produceMessage(topic: string, messages: [Message]) {
    if (!this.isProduerConnected) {
      const connectSuccess = await this.connect();

      if (!connectSuccess) {
        console.log(
          "[KAFKA Error] ",
          "The producer could not connect to the server. Please check the logs"
        );
        return;
      }
    }

    try {
      await this.producer.send({
        topic,
        messages,
        compression: CompressionTypes.GZIP,
      });
    } catch (err) {
      console.log("[KAFKA Error] ", err);
    }
  }

  public async disconnect() {
    await this.producer.disconnect();
  }
}
