import { NextFunction, Request, Response } from "express";
import { KafkaLogger } from "./KafkaLogger";

export const expressErrorLogger = (
  err: Error,
  req: Request,
  _: Response,
  next: NextFunction
) => {
  const kafkaLogger = new KafkaLogger(
    process.env.KAFKA_BROKERS?.split(",") || ["localhost:9091"]
  );

  const uuid = req.headers["x-request-uuid"] as string;

  const value = {
    requestId: uuid,
    controller: "education-service",
    timestamp: new Date().toISOString(),
    error: { msg: err.message, stack: err.stack, name: err.name },
  };
  kafkaLogger.produceMessage("request-logging", [
    { key: uuid, value: JSON.stringify(value) },
  ]);

  return next();
};
