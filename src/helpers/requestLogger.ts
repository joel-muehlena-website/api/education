import { NextFunction, Request, Response } from "express";
import { KafkaLogger } from "./KafkaLogger";

export const startRequestLogger = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const uuid = res.locals.uuid;

  if (uuid) {
    const brokers = process.env.KAFKA_BROKERS?.split(",");
    const kafkaLogger = new KafkaLogger(brokers || ["localhost:9091"]);
    const value = {
      timestamp: new Date().toISOString(),
      requestId: uuid,
      controller: "education-service",
      requestedPath: req.path,
      method: req.method,
      status: "START_CONTROLLER",
    };

    kafkaLogger.produceMessage("request-logging", [
      { key: uuid, value: JSON.stringify(value) },
    ]);
  }
  return next();
};

export const endRequestLogger = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const uuid = res.locals.uuid;
  if (uuid) {
    const brokers = process.env.KAFKA_BROKERS?.split(",");
    const kafkaLogger = new KafkaLogger(brokers || ["localhost:9091"]);
    const value = {
      timestamp: new Date().toISOString(),
      requestId: uuid,
      controller: "education-service",
      requestedPath: req.path,
      method: req.method,
      status: "EXIT_CONTROLLER",
    };
    kafkaLogger.produceMessage("request-logging", [
      { key: uuid, value: JSON.stringify(value) },
    ]);
  }
  return next();
};
