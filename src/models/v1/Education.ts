import { Schema, model } from "mongoose";

const EducationSchema = new Schema({
  schoolName: {
    type: String,
    required: true,
  },
  schoolLocation: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  from: {
    type: Number,
    required: true,
  },
  to: {
    type: Number,
  },
  current: {
    type: Boolean,
    default: false,
  },
  graduated: {
    type: Number,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

export const Education = model("educations", EducationSchema);
