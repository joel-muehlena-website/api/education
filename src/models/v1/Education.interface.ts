export interface Education {
  schoolName: string;
  schoolLocation: string;
  description: string;
  from: number;
  to: number;
  current: boolean;
  graduated: number;
  createdAt: Date;
  updatedAt: Date;
}
