import { NextFunction, Request, Response } from "express";
import { Education } from "../../models/v1/Education";
import { Education as IEducation } from "../../models/v1/Education.interface";

export const deleteEducation = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = req.params.id;

  const education: IEducation = await Education.findById(id);

  if (!education) {
    return res.status(404).json({
      code: 404,
      msg: `No eduaction entry with thw id ${id} found. Could not be edited.`,
    });
  }

  const delData: IEducation = await Education.findByIdAndDelete(id);

  if (delData) {
    res.status(200).json({ code: 200, msg: "Success", data: delData });
    return next();
  } else {
    res
      .status(500)
      .json({ code: 500, msg: "Failed to delete the education entry" });

    return next();
  }
};
