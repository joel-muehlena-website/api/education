import { NextFunction, Request, Response } from "express";
import { Education } from "../../models/v1/Education";
import { Education as IEducation } from "../../models/v1/Education.interface";

export const editEducation = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = req.params.id;

  const education: IEducation = await Education.findById(id);

  if (!education) {
    res.status(404).json({
      code: 404,
      msg: `No eduaction entry with thw id ${id} found. Could not be edited.`,
    });

    return next();
  }

  const {
    schoolName,
    schoolLocation,
    description,
    from,
    to,
    current,
    graduated,
  } = req.body;

  const updatedEdu: any = {};
  if (schoolName) updatedEdu.schoolName = schoolName;
  if (schoolLocation) updatedEdu.schoolLocation = schoolLocation;
  if (description) updatedEdu.description = description;
  if (from) updatedEdu.from = from;
  if (to) updatedEdu.to = to;
  if (current === true || current === false) updatedEdu.current = current;
  if (graduated) updatedEdu.graduated = graduated;
  updatedEdu.updatedAt = new Date();

  console.log(updatedEdu);

  const updatedEduData = await Education.findByIdAndUpdate(
    id,
    { $set: updatedEdu },
    { new: true }
  );

  if (updatedEduData) {
    res
      .status(200)
      .json({ msg: 200, message: "Success", data: updatedEduData });

    return next();
  } else {
    res
      .status(500)
      .json({ code: 500, msg: "Failed to update the education entry" });

    return next();
  }
};
