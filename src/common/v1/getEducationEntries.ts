import { NextFunction, Request, Response } from "express";
import { Education } from "../../models/v1/Education";
import { Education as IEducation } from "../../models/v1/Education.interface";

export const getEducation = async (
  _: Request,
  res: Response,
  next: NextFunction
) => {
  const education: IEducation[] = await Education.find();

  if (education.length <= 0) {
    res.status(404).json({ code: 404, msg: "No eduaction entry found" });
    return next();
  }

  res.json({ code: 200, msg: "Success", data: education });
  return next();
};

export const getEducationById = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = req.params.id;

  const education: IEducation = await Education.findById(id);

  if (!education) {
    res
      .status(404)
      .json({ code: 404, msg: `No eduaction entry with thw id ${id} found` });
    return next();
  }

  res.json({ code: 200, msg: "Success", data: education });
  return next();
};
