import { NextFunction, Request, Response } from "express";
import { Education } from "../../models/v1/Education";
import { Education as IEducation } from "../../models/v1/Education.interface";

export const createEducation = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const {
    schoolName,
    schoolLocation,
    description,
    from,
    to,
    current,
    graduated,
  } = req.body;

  const date = new Date();
  const newEducationEntry: IEducation = {
    schoolName,
    schoolLocation,
    description,
    from,
    to,
    current,
    graduated,
    createdAt: date,
    updatedAt: date,
  };

  const saveEducationEntry = await new Education(newEducationEntry).save();

  if (saveEducationEntry) {
    res
      .status(200)
      .json({ msg: 200, message: "Success", data: saveEducationEntry });

    return next();
  } else {
    res
      .status(500)
      .json({ code: 500, msg: "Failed to create the education entry" });

    return next();
  }
};
