import { NextFunction, Request, Response } from "express";
import morgan from "morgan";
import { configureServerAndEnv } from "@joel-muehlena-website/jm-config-server-addon";
import fs from "fs";
import path from "path";
import dotenv from "dotenv";

import { ExpressServer } from "./Server";
import { MongoDBConnection } from "./config/mongoDBSetup";
import { educationV1Router } from "./routes/v1/education";
import { expressErrorLogger } from "./helpers/expressErrorLogger";
import { endRequestLogger, startRequestLogger } from "./helpers/requestLogger";

const configServer = `http://${
  process.env.NODE_ENV === "production"
    ? "config-server.default.svc.cluster.local"
    : "172.30.204.237"
}:8081/config?filename=education:${
  process.env.NODE_ENV === "production" ? "prod" : "dev"
}`;

(async () => {
  try {
    await configureServerAndEnv(configServer, path.join(__dirname, "/.env"));
  } catch (err) {
    console.log("Error fetching config server");
  }
})();

let count = 0;
let i = setInterval(() => {
  if (count > 5000) {
    console.log("Config Timeout");
    process.exit(-1);
  }
  fs.access(path.join(__dirname, "/.env"), fs.constants.F_OK, (err) => {
    if (!err) {
      clearInterval(i);
      runApp();
    } else {
      count++;
    }
  });
}, 500);

const runApp = () => {
  dotenv.config({ path: path.join(__dirname, "/.env") });
  const PORT: number = parseInt(process.env.PORT as string) || 3001;

  let expressServer = new ExpressServer(PORT);

  //Configure middleware
  expressServer.addMiddleware(morgan, "dev");

  //Configure Routes
  expressServer.get("/healthz", (_: Request, res: Response) => {
    res.sendStatus(200);
  });

  expressServer.addMiddleware(
    (req: Request, res: Response, next: NextFunction) => {
      res.locals.uuid = req.headers["x-request-uuid"];
      next();
    }
  );

  //Start logging
  expressServer.addMiddleware(startRequestLogger);

  //Education v1 routes
  expressServer.addRouteGroup("/v1", educationV1Router);

  //End logging
  expressServer.addMiddleware(endRequestLogger);

  //Error Logger for default errors
  expressServer.addMiddleware(expressErrorLogger);

  expressServer.get("*", (_: Request, res: Response) => {
    if (!res.headersSent) {
      res.status(400).json({ code: 400, msg: "This operation is not allowed" });
      throw Error("Operation not allowed");
    }
  });

  expressServer.run();

  //Connect to Atlas cluster
  let dbConnection = new MongoDBConnection();
  dbConnection.connectToMongoDB();
};
